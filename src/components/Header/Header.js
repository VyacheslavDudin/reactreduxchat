import React from 'react';
import './Header.css';
import { connect } from "react-redux";

export function Header(props) {
    const { messages } = props;
    const messagesCount = messages.length;

    const participants = new Set();
    messages.forEach(msg => participants.add(msg.user));
    const participantsCount = participants.size;

    const sortDates = (dateA, dateB) => dateB - dateA;
    const lastMessageDate = messages.map(msg => new Date(msg.createdAt)).sort(sortDates)[0];
    const dateWithoutWeekDay = lastMessageDate.toDateString().substr(lastMessageDate.toDateString().indexOf(' '));
    const [,month, day, year] = dateWithoutWeekDay.split(' ');
    const time = `${lastMessageDate.getHours() + ':' + lastMessageDate.getMinutes()}`;
    const formattedDate = [day, month, year, time].join(' ');
    return (
        <div className="header">
            <div className="header__main-info">
                <span className="font-bold">My chat</span>
                <span className="font-italic">{participantsCount} participants</span>
                <span className="font-italic">{messagesCount} messages</span>
            </div>
            <span>last message at {formattedDate}</span>
        </div>
    );
}

function mapStateToProps(state) {
    return {
      messages: state.messages
    }
  }
  
export default connect(mapStateToProps)(Header);
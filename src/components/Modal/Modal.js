import React, {useEffect} from 'react';
import './Modal.css';


export function Modal({ onEditMode, onClose }) {
    const onCloseHandler = event => {
        const formElems = document.querySelectorAll('.message__input-form_modal *');
        if (Array.from(formElems).some(el => el === event.target)) {
            return;
        }
        else {
            document.removeEventListener('click', onCloseHandler);
            onClose();
        };
    }
    document.querySelector('.chat').style.overflow = 'hidden';
    document.addEventListener('click', onCloseHandler);
    useEffect(() => () => {
        document.querySelector('.chat').style.overflow = 'auto';
        document.removeEventListener('click', onCloseHandler);
    });
    const onEdit = () => {
        const newMessage = document.querySelector('.message__input_modal').value;
        if (!newMessage || !newMessage.trim()) return;
        document.querySelector('.message__input').value = '';
        onEditMode(newMessage); 
    }
    return (
        <div className="message__input-form message__input-form_modal">
            <textarea
                autoFocus={true}
                className="message__input message__input_modal"
                placeholder="Edit Message"
                resize="none"
            >
            </textarea>
            <button className="button-send" onClick={onEdit}><i className="fas fa-check-circle"></i></button>
        </div>
    );
}
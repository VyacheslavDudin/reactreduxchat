import React from 'react';
import { connect } from "react-redux";
import './MessageInput.css';
import { bindActionCreators } from 'redux';
import { addMessageAction } from './../../containers/Chat/action';


export function MessageInput({ addMessage }) {
    const onSend = () => {
        const newMessage = document.querySelector('.message__input').value;
        if (!newMessage || !newMessage.trim()) return;
        document.querySelector('.message__input').value = '';
        addMessage(newMessage); 
    };
    return (
        <div className="message__input-form">
            <textarea
                autoFocus={true}
                className="message__input"
                placeholder="Message"
                resize="none"
            >
            </textarea>
            <button className="button-send" onClick={onSend}><i className="fas fa-arrow-circle-up"></i></button>
        </div>
       
    );
}

const actions = { 
    addMessage: addMessageAction
}

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(null, mapDispatchToProps)(MessageInput);
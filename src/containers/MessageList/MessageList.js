import React, { useState } from 'react';
import './MessageList.css';
import Message from '../../components/Message/Message';
import MessageInput from '../../components/MessageInput/MessageInput';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
import { editMessageAction } from '../Chat/action';
import { Modal } from '../../components/Modal/Modal';

export function MessageList(props) {
    let { messages, likeRepository, editMessage, currentUser } = props;
    let [msgId, setMsgId] = useState('');
    const sortByDateAsc = (msg1, msg2) => new Date(msg1.createdAt) - new Date(msg2.createdAt);
    let sortedMessages = messages.sort(sortByDateAsc).map(msg => ({ ...msg, createdAt: new Date(msg.createdAt) }) );

    const onEditMode = newMessage => {
        editMessage(msgId, newMessage);
        setMsgId('')
    }
    const onEdit = (id, text) => {
        setMsgId(id);
    }
    const onClose = () => setMsgId('');
    const onKeyDown = event => {
        if (event.key !== 'ArrowUp') {
            return;
        }
        else {
            const userMessages = messages.filter(msg => msg.userId === currentUser.userId);
            const lastMessage = userMessages.sort(sortByDateAsc)[userMessages.length -1];
            onEdit(lastMessage.id, '');
        }
    }
    document.addEventListener('keydown', onKeyDown);
    sortedMessages = sortedMessages.map(msg => 
        <Message 
            message={{
                avatar: msg.avatar,
                text: msg.text,
                user: msg.user,
                createdAt: msg.createdAt,
                msgId: msg.id
            }}
            key={msg.id}
            onEdit={(id, text) => onEdit(id, text)}
            likesCount={(likeRepository || []).filter(el => el.id ===msg.id).length}
        />)
    return (
        <>
            <div className="messages">
                {sortedMessages}
            </div>
            <MessageInput/>
            {msgId ? <Modal onEditMode={newMessage => onEditMode(newMessage)} onClose={onClose}/> : null}
        </>
    );
}

const actions = { 
    editMessage: editMessageAction
};

function mapStateToProps(state) {
    return {
      messages: state.messages,
      likeRepository: state.likeRepository,
      currentUser: state.currentUser
    }
  }

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
  
export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
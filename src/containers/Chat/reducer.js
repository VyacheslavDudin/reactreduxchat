import {
    EDIT_MESSAGE,
    LIKE_MESSAGE,
    ADD_MESSAGE,
    DELETE_MESSAGE
  } from './actionTypes';
  
  export default (state = {}, action) => {
    switch (action.type) {
      case ADD_MESSAGE:
        const {user, userId, avatar} = state.currentUser;
        const newMessage = {
            id: new Date().toISOString(),
            text: action.message,
            createdAt: new Date().toISOString(),
            editedAt: '',
            user,
            userId,
            avatar    
        };
        return {
          ...state,
          messages: [...(state.messages || []), newMessage]
        };
      case EDIT_MESSAGE:  
        const editedMessage = {
            ...(state.messages.filter(msg => msg.id === action.id)[0]),
            text: action.messageText,
            editedAt: new Date().toISOString()
        }
        return {
            ...state,
            messages: [...(state.messages.filter(msg => msg.id !== editedMessage.id) || []), editedMessage]
        };
      case DELETE_MESSAGE:
        return {
            ...state,
            messages: state.messages.filter(msg => msg.id !== action.id)
        };
      case LIKE_MESSAGE:
        const {userId: authorId} = state.currentUser;
        const {id} = action;

        if ((state.likeRepository || []).some(like => like.userId === authorId && like.id === id)) {
            return {
                ...state,
                likeRepository: (state.likeRepository || []).filter(like => like.id !== id || like.userId !== authorId)
            };
        }
        else {
            return {
                ...state,
                likeRepository: [...(state.likeRepository || []), {id, userId: authorId}]
            };
        }
      default:
        return state;
    }
  };
  
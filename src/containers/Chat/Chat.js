import React, { Component } from 'react';
import './Chat.css';
import Header from './../../components/Header/Header';
import Spinner from './../../components/Spinner/Spinner';
import MessageList from '../MessageList/MessageList';

export class Chat extends Component {
  constructor(props) {
    super(props);
    this.apiPath = 'https://edikdolynskyi.github.io/react_sources/messages.json';
    // Since it does not affect any other components and it`s allowed to use local state
    // instead of clogging global state, I use local state for 'isLoaded'
    this.state = {
      isMessageListLoaded: false
    }
  }
  
  
  componentDidMount() {
    this.setState({isMessageListLoaded: true});
  }

  render() {
    const { isMessageListLoaded: isLoaded } = this.state;

    return (
      <div className="chat">
          {
          isLoaded 
          ?
            <>
              <Header className="chat-header"/>
              <MessageList/>
            </>
          : <Spinner/>
          }
      </div>
    );
  }
}

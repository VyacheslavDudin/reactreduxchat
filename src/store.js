import { createStore } from 'redux';
import reducer from './containers/Chat/reducer';
import { initialMessages } from './initialMessages';


const initialState = {
    currentUser: {
        user:'Ben',
        userId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
        avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
      },
    messages: initialMessages,
    isMessageListLoaded: false,
    likeRepository: []
};

const store = createStore(reducer, initialState);

export default store;